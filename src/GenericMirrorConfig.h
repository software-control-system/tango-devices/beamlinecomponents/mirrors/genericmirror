#ifndef MIRROR_CONFIG_H
#define MIRROR_CONFIG_H


namespace MIRROR
{
	static const string NAME							="MIRROR";
	static const double PSEUDO_BENDER_VAL				= 0.0;
	static const double MINIMAL_PSEUDO_BENDER			= -20.0;
	static const double MAXIMAL_PSEUDO_BENDER			= 20.0;

	static const double ASYMMETRY_VAL					= 0.0;
	static const double MINIMAL_ASYMMETRY				= -20.0;
	static const double MAXIMAL_ASYMMETRY				= 20.0;

	static const double CURVATURE_RADIUS_VAL			= 0.0;
	static const double MINIMAL_CURVATURE				= -1000;
	static const double MAXIMAL_CURVATURE				= 1000.0;

	static const bool	USE_EQUATION					= true;

	static const double P								= 5.5;
	static const double Q								= 15.5;
	static const unsigned short STRIP_NUMBER			= 1;

	static const string	BENDER_DEVICE_NAME				= "tmp/bender/bender";
	static const string	ATTRIBUTE_CURVATURE_NAME		= "Strip 1";

	static const bool HAS_BENDER						= false;
	static const bool HAS_CHANGE_STRIP_MOTOR			= false;

	namespace BENDER1
	{
		static const string DESCRIPTION						= "Bender 1";
		static const double VAL								= 0.0;
		static const double MINIMAL_VAL						= -20.0;
		static const double MAXIMAL_VAL						= 20.0;
		static const double CURVATURE_CONSTANT_A1			= 39.0*0.056;
		static const double	CURVATURE_CONSTANT_B1			= 0.0;

		static const string TABLE_PATH						= "D:\\Table.txt";
		static const int	TABLE_FIRST_INDEX				= 0;
		static const int	TABLE_SECOND_INDEX				= 1;
		
		static const string INVERSE_TABLE_PATH				= "D:\\Table2.txt";
		static const int	INVERSE_TABLE_FIRST_INDEX		= 0;
		static const int	INVERSE_TABLE_SECOND_INDEX		= 1;
	}
	

	namespace DEVICE_INIT_PROPERTIES
	{
			static const string COMMAND_STOP_NAME				= "STOP";
			static const string COMMAND_STATE_NAME				= "State";
			static const string ATTRIBUTE_POSITION_NAME			= "position";
			static const string ATTRIBUTE_THETA_NAME			= "position";

			static const string THETA_MOTOR_NAME				= "tmp/diffabs/theta";
			static const string CHANGE_STRIP_MOTOR_NAME			= "tmp/diffabs/tx";
			static const unsigned short	MIRROR_TYPE				= 0;
			//static const string BENDER_DEVICE_NAME				= "tmp/twomotorsbender/bender";
			//static const string BENDER_ATTRIBUTE_NAME			= "bender";
			//static const string BENDER_1_ATTRIBUTE_NAME			= "bender1";
			//static const string BENDER_2_ATTRIBUTE_NAME			= "bender2";
			//static const string BENDER_RADIUS_ATTRIBUTE_NAME	= "benderCurvatureRadius";
			//static const string	BENDER_ASSYMETRY_ATTRIBUTE_NAME = "assymetry";
	}
	

}



#endif

